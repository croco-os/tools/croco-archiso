<p align="center">
<a href="https://croco-os.io"><img src="https://raw.githubusercontent.com/croco-os-os/croco-os-packages/main/croco-os-artworks/files/logo/png/logo-circle/logo-circle-1.png" height="128" width="128" alt="Croco-OS"></a>
</p>

<p align="center">
<a href="https://www.buymeacoffee.com/adi1090x"><img width="32px" src="https://raw.githubusercontent.com/adi1090x/files/master/other/1.png" alt="Buy Me A Coffee"></a>
<a href="https://ko-fi.com/adi1090x"><img width="32px" src="https://raw.githubusercontent.com/adi1090x/files/master/other/2.png" alt="Donate for Croco-OS on ko-fi"></a>
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6VETHHYHXESRN"><img width="32px" src="https://raw.githubusercontent.com/adi1090x/files/master/other/3.png" alt="Donate for Croco-OS via Paypal"></a>
</p>

<p align="center">
  <img src="https://img.shields.io/badge/Maintained%3F-Yes-green?style=flat-square">
  <img src="https://img.shields.io/github/downloads/croco-os-os/releases/total?label=downloads&logo=github&color=blue&style=flat-square">
  <img src="https://img.shields.io/sourceforge/dt/croco-os.svg?label=downloads&logo=sourceforge&color=teal&style=flat-square">
  <img src="https://img.shields.io/github/stars/croco-os-os/croco-os?style=flat-square">
  <img src="https://img.shields.io/github/issues/croco-os-os/croco-os?color=violet&style=flat-square">
</p>

<p align="center">
Yet another minimal linux distribution, based on <a href="https://www.archlinux.org">Arch Linux</a>.
</p>

<p align="center">
  <a href="https://croco-os.io" target="_blank"><img alt="home" src="https://img.shields.io/badge/HOME-blue?style=flat-square"></a>
  <a href="https://wiki.croco-os.io" target="_blank"><img alt="wiki" src="https://img.shields.io/badge/WIKI-blue?style=flat-square"></a>
  <a href="https://croco-os.io/gallery" target="_blank"><img alt="screenshots" src="https://img.shields.io/badge/SCREENSHOTS-blue?style=flat-square"></a>
  <a href="https://www.reddit.com/r/croco-os" target="_blank"><img alt="reddit" src="https://img.shields.io/badge/REDDIT-blue?style=flat-square"></a>
  <a href="https://discord.gg/3PzeJ5S7Pu" target="_blank"><img alt="discord" src="https://img.shields.io/badge/DISCORD-blue?style=flat-square"></a>
  <a href="https://t.me/croco-osos" target="_blank"><img alt="telegram" src="https://img.shields.io/badge/TELEGRAM-blue?style=flat-square"></a>
  <a href="https://matrix.to/#/#croco-os:matrix.org" target="_blank"><img alt="matrix" src="https://img.shields.io/badge/MATRIX-blue?style=flat-square"></a>
</p>

![img](https://raw.githubusercontent.com/croco-os-os/core-packages/main/calamares-config/files/calamares/branding/croco-os/welcome.png)

---

### From Croco-OS Wiki

- [Things To Do After Installing Croco-OS OS](https://wiki.croco-os.io/installation/post_install)
- [Build Croco-OS ISO With Its Source](https://wiki.croco-os.io/misc/build_iso)
- [Install Croco-OS With Calamares (With Encryption)](https://wiki.croco-os.io/installation/calamares)
- [Install Croco-OS On UEFI System (With Encryption)](https://wiki.croco-os.io/installation/abif/uefi)
- [Install Croco-OS On BIOS System (With Encryption)](https://wiki.croco-os.io/installation/abif/bios)
- [Create A Bootable USB With Croco-OS](https://wiki.croco-os.io/installation/boot/usb)
- [Boot Croco-OS ISO With GRUB2 Bootloader](https://wiki.croco-os.io/installation/boot/grub)

### Get The ISO

You can download latest release of `ISO` (see [`releases`](https://github.com/croco-os-os/releases)), Available options are...
<p align="center">
  <a href="https://github.com/croco-os-os/releases/releases/download/v21.10/croco-os-2021.10.05-x86_64.iso" target="_blank"><img alt="undefined" src="https://img.shields.io/badge/Download-Github-blue?style=for-the-badge&logo=github"></a>&nbsp;&nbsp;
  <a href="https://sourceforge.net/projects/croco-os/files/latest/download" target="_blank"><img alt="undefined" src="https://img.shields.io/badge/Download-Sourceforge-orange?style=for-the-badge&logo=sourceforge"></a>&nbsp;&nbsp;
  <a href="https://github.com/croco-os-os/releases/releases/download/v21.10/croco-os-2021.10.05-x86_64.iso.torrent" target="_blank"><img alt="undefined" src="https://img.shields.io/badge/Download-Torrent-magenta?style=for-the-badge&logo=discogs"></a>
</p>


> Default `username` and `password` is **liveuser**.

> Don't forget to hit the **`STAR`** if you love Croco-OS.
