#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="croco-os"
iso_label="croco_0.14.12"
iso_publisher="GiacoLenzo2109 <http://www.github.com/GiacoLenzo2109>"
iso_application="Croco-OS Live/Installation/Rescue CD"
iso_version="0.14.12"
install_dir="arch"
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
file_permissions=(
  ["/etc/shadow"]="0:0:0400"
  ["/etc/gshadow"]="0:0:0400"
  ["/etc/sudoers.d"]="0:0:750"
  ["/root"]="0:0:750"
  ["/root/.automated_script.sh"]="0:0:755"
  ["/root/customize_airootfs.sh"]="0:0:755"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/Installation_guide"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/usr/local/bin/croco-all-cores"]="0:0:755"
  ["/usr/local/bin/croco-final"]="0:0:755"
  ["/usr/local/bin/croco-remove-nvidia"]="0:0:755"
  ["/usr/local/bin/croco-displaymanager-check"]="0:0:755"
  ["/usr/local/bin/croco-before"]="0:0:755"
)
